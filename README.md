## Sonar Cloud - Microsserviço Externo

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-externo)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-externo&metric=bugs)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-externo)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-externo&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-externo)

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-externo&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-externo)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-externo&metric=coverage)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-externo)

[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-externo&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-externo)
